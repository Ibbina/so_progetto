#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
//La sem_close() chiude un semaforo che è stato aperto precedentemente con una sem_open() e rilascia ogni risorsa associata al semaforo

void internal_semClose(){
  // do stuff :)


	int cur = running->syscall_args[0]; //prendo fd dal pcb, ovvero la risorsa da chiudere
	SemDescriptor* semd = SemDescriptorList_byFd(&running->sem_descriptors, cur); //semd sarà il sem descriptor del semaforo in questione da chiudere
//gestione errore in caso il descrittore non sia contenuto dal processo
	if(!semd){
		running->syscall_retvalue=DSOS_ESEMCLOSE;
		disastrOS_debug("Il processo non contiene il sem descriptor:%d\n",cur);
		return;}

	semd = (SemDescriptor*) List_detach(&running->sem_descriptors, (ListItem*)semd); //rimuovo il sem descriptor dalla lista del processo poichè devo poi liberarne tutte le risorse correlate

	if(!semd){
		running->syscall_retvalue=DSOS_ESEMCLOSE;
		disastrOS_debug("Errore nella rimozione del descrittore:%d\n");
		return;}
	Semaphore* sem = semd->semaphore; //prendo il semaforo
//si fa lo stesso per il puntatore al descrittore
	SemDescriptorPtr* semdptr = (SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*)(semd->ptr));

	if(!semdptr){
		running->syscall_retvalue=DSOS_ESEMCLOSE;
		disastrOS_debug("Errore nella rimozione del puntatore al descrittore:%d\n");
		return;}

	SemDescriptor_free(semd); //libero la memoria occupata dal descriptor

	SemDescriptorPtr_free(semdptr);
	//Semaphore_free(sem);
	running->last_sem_fd--;
	if(sem->waiting_descriptors.size ==0 && sem->descriptors.size==0){
		sem = (Semaphore*)List_detach(&semaphores_list,(ListItem*)sem);
		if(!sem){
			running->syscall_retvalue= DSOS_ESEMCLOSE;
			return;
		}
		Semaphore_free(sem);
	}
	running->syscall_retvalue=0;


}

