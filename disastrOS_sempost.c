#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
/*sem_post() increments (unlocks) the semaphore pointed to by sem. If the semaphore's value consequently becomes greater than zero, then another process or thread blocked in a sem_wait(3) call will be woken up and proceed to lock the semaphore.
Return Value
sem_post() returns 0 on success; on error, the value of the semaphore is left unchanged, -1 is returned, and errno is set to indicate the error.*/
void internal_semPost(){
	int fd = running->syscall_args[0];
	SemDescriptor* semd = (SemDescriptor*) SemDescriptorList_byFd(&running->sem_descriptors, fd);
	if(!semd){
		disastrOS_debug("Il descrittore non è presente nella lista dei descrittori del processo in running\n");
		running->syscall_retvalue= DSOS_ESEMPOST;
		return;
	}
	Semaphore* sem = (Semaphore*)semd->semaphore;
	if(!sem){
		disastrOS_debug("Errore nel prendere il semaforo\n");
		running->syscall_retvalue = DSOS_ESEMPOST;
		return;
	}
	sem->count++;
	SemDescriptorPtr* semdptr;
	if(sem->count >0){//Tocca al processo preso entrare in stato ready
		if(sem->waiting_descriptors.first){
		sem->count--;
		semdptr = (SemDescriptorPtr*)List_detach(&sem->waiting_descriptors, (ListItem*) sem->waiting_descriptors.first); //elimino il puntatore al descrittore del semaforo dalla waiting list
		/*if(!semdptr){
			
			running->syscall_retvalue=DSOS_ESEMPOST;
			disastrOS_debug("Errore nella rimozione del descrittore:%d\n");
		}*/
		List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) semdptr);
		semd = semdptr->descriptor;

		PCB* next_p = semd->pcb; //metto in esecuzione il processo rimosso precedentemente
		if(!next_p){
			disastrOS_debug("Errore");
			running->syscall_retvalue = DSOS_ESEMPOST;
			return;
		}
		next_p->status = Ready;
		List_detach(&waiting_list, (ListItem*)semd->pcb); 
		List_insert(&ready_list, ready_list.last, (ListItem*)next_p); //inserisco il processo nella ready list
	}
	}
	running->syscall_retvalue =0;
	return;
  // do stuff :)
}

