#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"
/*sem_wait() decrements (locks) the semaphore pointed to by sem.  If
       the semaphore's value is greater than zero, then the decrement
       proceeds, and the function returns, immediately.  If the semaphore
       currently has the value zero, then the call blocks until either it
       becomes possible to perform the decrement (i.e., the semaphore value
       rises above zero), or a signal handler interrupts the call.*/

void internal_semWait(){
	int fd = running->syscall_args[0];
	SemDescriptor* semd = (SemDescriptor*)SemDescriptorList_byFd(&running->sem_descriptors, fd);
	
	if(!semd){
		disastrOS_debug("Il descrittore ricercato non è presente nella lista dei descrittori del processo in running\n");
		running->syscall_retvalue= DSOS_ESEMWAIT;
		return;
	}
	Semaphore* sem = semd->semaphore;
	//SemDescriptorPtr* semdptr = semd->ptr;
	if(!sem){
		disastrOS_debug("Errore nel prendere il semaforo\n");
		running->syscall_retvalue = DSOS_ESEMWAIT;
		return;
	}
	SemDescriptorPtr* semdptr = semd->ptr;
	if(sem->count>0){
		//posso decrementare il contatore del semaforo
		sem->count--;
	}else{//significa che non posso decrementare il contatore quindi il processo non sarà più in running ma dovrà essere messo in waiting
		List_detach(&sem->descriptors, (ListItem*)semdptr);//rimuovo il descrittore dalla lista dei descrittori 
		List_insert(&sem->waiting_descriptors, sem->waiting_descriptors.last, (ListItem*)semdptr); //inserisco il semaforo in quelli in stato waiting tramite puntatore al descrittore
		PCB* n =(PCB*)List_detach(&ready_list, ready_list.first);
		if(!n){ 
			running->syscall_retvalue = DSOS_ESEMWAIT;
			return;}
		 //se non c'è nessun processo in ready da errore
		List_insert(&waiting_list, waiting_list.last, (ListItem*)running);//metto il processo corrente in waiting
		running->status = Waiting;
		running = n; //metto in running il primo processo in stato ready
	

	}
	running->syscall_retvalue =0;
	return;

  // do stuff :)
}

