#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semOpen(){
  // do stuff :)


	//printf("Sono nella semOpen\n");
	int fd = running->syscall_args[0]; //prendo fd
	int mode = running->syscall_args[1]; //prendo mode
	int value = running->syscall_args[2]; //prendo value

	//Il valore di un semaforo deve essere necessariamente positivo quindi
	if(value<0){
		
		disastrOS_debug("Errore nella semOpen, il valore passato non è valido\n");
		running->syscall_retvalue = DSOS_ESEMOPEN;
		return;
	}
	SemDescriptor* semd;
	Semaphore* sem = SemaphoreList_byId(&semaphores_list, fd); //cerco se il semaforo con tale id già è nella lista dei semafori
	//if(sem!=0){ //il semaforo è presente già nella lista dei semafori
// If O_EXCL and O_CREAT are set, and path names a symbolic link, open() shall fail and set errno to [EEXIST], regardless of the contents of the symbolic link. If O_EXCL is set and O_CREAT is not set, the result is undefined.
		/*if(sem!=0 ){
			disastrOS_debug("Semaforo già presente");
			running->syscall_retvalue = DSOS_ESEMOPEN;
		return;}else*/ if((mode&DSOS_CREATE) == DSOS_CREATE ) {
			if ((mode&DSOS_EXCL)==DSOS_EXCL && sem!=0){
				//se è stato passato un argomento corretto si allerta che il semaforo è già esistente
			disastrOS_debug(" EXCL specificata");
			running->syscall_retvalue = DSOS_ESEMOPEN;
			return;
			}else if(!sem ){
				sem = Semaphore_alloc(fd, value);
				List_insert(&semaphores_list, semaphores_list.last, (ListItem*)sem);
			}
		}else{
			printf("else");
			disastrOS_debug("L'argomento passato alla semopen non è corretto");
			running->syscall_retvalue = DSOS_ESEMOPEN;
		return;
		}
		
	/*}else{//se il semaforo non era già esistente si alloca e si inserisce nella lista dei semafori
	/*if((mode&DSOS_CREATE)==DSOS_CREATE){
		sem = Semaphore_alloc(fd, value);
		//List_insert(&semaphores_list, semaphores_list.last, (ListItem*)sem);
		if(!sem){
			disastrOS_debug("Problema nell'allocazione del semaforo");
			running->syscall_retvalue = DSOS_ESEMOPEN;
		return;
		}
		List_insert(&semaphores_list, semaphores_list.last, (ListItem*)sem);
	}*/
	//}
	if(!sem){
		running->syscall_retvalue = DSOS_ESEMOPEN;
		return;
	}	
	semd = SemDescriptor_alloc(running->last_sem_fd, sem,running);	
	if(!semd){
		disastrOS_debug("Problema nell'allocazione del descrittore del semaforo");
		running->syscall_retvalue = DSOS_ESEMOPEN;
		
		return;
	}
	running-> last_sem_fd++; //aggiorno numero semafori
	SemDescriptorPtr* semdptr = SemDescriptorPtr_alloc(semd);//Alloco puntatore al descrittore del semaforo
	semd->ptr = semdptr; 
	List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*)semd);
	List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*)semdptr);
	running->syscall_retvalue = semd->fd; //ritorno fd semaforo
	return;


}

